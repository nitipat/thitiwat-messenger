﻿#!/usr/bin/python
#-*- coding: utf-8 -*-
 
from Tkinter import *
import threading, socket, tkMessageBox, re

class ChatClientApp:
	def __init__(self, master):
		self.root = master
		self.LoginPage()

	def LoginPage(self):
		''' Title '''
		self.root.title("Login - Thitiwat Messenger")

		''' new main frame '''
		self.frame = Frame(self.root)

		# debug mode
		#self.username = ''
		#self.ChatWindowPage(0)
		#return

		''' Login Background '''
		self.c = Canvas(self.frame, width=500, height=300)
		self.image = PhotoImage(file="images/room/LoginBG.gif")
		self.label1 = Label(self.c,image=self.image)
		self.label1.image = self.image
		self.label1.place(x=0, y=0)  
 
		''' User form '''
		self.form_user = Entry(self.c, width=10, bg="#b6f0f8", justify="center", bd=0, font=("Helvetica",25,""))
		self.form_user.insert(0, "")		
		self.form_user.place(x=290, y=45) 

		''' Host form '''
		self.form_host = Entry(self.c, width=10, bg="#b6f0f8", justify="center", bd=0, font=("Helvetica",20,""))
		self.form_host.insert(0, socket.gethostbyname(socket.gethostname()))		
		self.form_host.place(x=180, y=120) 

		''' Port form '''
		self.form_port = Entry(self.c, width=5, bg="#b6f0f8", justify="center", bd=0, font=("Helvetica",20,""))
		self.form_port.insert(0, "8000")
		self.form_port.place(x=179, y=170) 

		''' Login Button '''
		self.image = PhotoImage(file="images/room/LoginButton.gif")
		self.btn_login = Label(self.c, image=self.image, bd=0)
		self.btn_login.image = self.image
		self.btn_login.place(x=290, y=160) 
		self.btn_login.bind("<Button>", self.LoginButton)

		''' Add page to main frame '''
		self.frame.grid()
		self.c.grid(row=0, column=0)

	def ChatWindowPage(self, socket):
		self.emoticons_icon = []
		''' hide login frame '''
		self.frame.grid_forget()
		''' title '''
		self.root.title("Chat room - Thitiwat Messenger")
		''' set new connection '''
		self.connection = socket
		''' set new "to" string '''
		self.tospecial = ''
		''' new frame '''
		self.frame2 = Frame(self.root)

		''' Chat Background '''
		self.c2 = Canvas(self.frame2, width=800, height=500)
		self.image = PhotoImage(file="images/room/MainChatBG.gif")
		self.label2 = Label(self.c2, image=self.image)
		self.label2.image = self.image
		self.label2.place(x=0, y=0)  

		''' Username show '''
		self.showname = Label(self.frame2, text=self.username, bg="#c8f2ff", font=("Helvetica",20,""))
		self.showname.place(x=80, y=55)

		''' Chat Textarea'''
		self.frame3 = Frame(self.frame2)
		self.textarea = Text(self.frame3, height=18, width=65, bd=0, bg="#c8f2ff")
		#self.textarea.place(x=40, y=119) 
		self.scroll = Scrollbar(self.frame3, command=self.textarea.yview, bd=0)
		self.textarea.configure(yscrollcommand=self.scroll.set)
		self.textarea.tag_configure('bold', font=('Arial', 16, 'bold'))
		self.textarea.tag_configure('italic', font=('Arial', 16, 'italic'), foreground='#888')
		self.textarea.tag_configure('normal', font=('Arial', 16))
		self.textarea.config(state=DISABLED)
		#self.scroll.place(x=560, y=119) 
		self.frame3.grid()
		self.textarea.grid(row=0, column=0)
		self.scroll.grid(row=0, column=1, sticky=N+S)
		self.frame3.place(x=40, y=119)
		

		''' Chat box '''
		self.text_ent = Entry(self.frame2, width=48, bd=0, font=('Arial', 15))
		self.text_ent.bind("<Return>", self.EnterToSend)
		self.text_ent.place(x=40, y=423) 

		''' User online '''
		self.listonline = Listbox(self.frame2, height=13, width=17, activestyle='none', bd=0, bg="#c8f2ff", font=('Arial', 12))
		self.listonline.pack(side=LEFT)
		self.listonline.bind('<<ListboxSelect>>', self.onselectUser)
		self.listonline.place(x=608, y=150) 

		''' Logout Button '''
		self.image = PhotoImage(file="images/room/LogoutButton.gif")
		self.btn_logout = Label(self.frame2, image=self.image, bd=0)
		self.btn_logout.image = self.image
		self.btn_logout.place(x=420, y=44) 
		self.btn_logout.bind("<Button>", self.LogoutButton)

		''' About Button '''
		self.image = PhotoImage(file="images/room/AboutButton.gif")
		self.btn_about = Label(self.frame2, image=self.image, bd=0)
		self.btn_about.image = self.image
		self.btn_about.place(x=322, y=44) 
		self.btn_about.bind("<Button>", self.aboutButton)

		''' Send Button '''
		self.image = PhotoImage(file="images/room/SendButton.gif")
		self.btn_about = Label(self.frame2, image=self.image, bd=0)
		self.btn_about.image = self.image
		self.btn_about.place(x=596, y=412) 
		self.btn_about.bind("<Button>", self.EnterToSend)

		''' Emoticons menu '''
		self.emoticons = {}
		for i in range(1, 17):
			self.image = PhotoImage(file="images/emoticon/"+str(i)+".gif")
			self.emoticons[i] = Label(self.frame2, image=self.image, bd=0)
			self.emoticons[i].image = self.image
			self.emoticons[i].value = i
			self.emoticons[i].place(x=35+((i-1)*32), y=452) 
			self.emoticons[i].bind("<Button>", self.AddEmoticonButton)

		''' Add page to main frame '''
		self.frame2.grid()
		self.c2.grid(row=0, column=0)

	def login(self):
		if self.form_user.get().encode("utf-8") == '':
			tkMessageBox.showerror("Error", "Plese insert your username...")
			return
		try:
			new_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			new_socket.connect((self.form_host.get(), int(self.form_port.get())))
		except:
			tkMessageBox.showerror("Error", "Connection error...")
			return

		self.FetchData = FetchNewData(new_socket)
		self.FetchData.start()
		''' set username '''
		self.username = self.form_user.get().encode("utf-8")
		self.ChatWindowPage(new_socket)
		''' add username to server '''
		self.connection.send("ADDUSER "+self.username)

	def logout(self):
		self.connection.send("END")
		self.FetchData.stop()
		#self.root.quit()
		self.frame2.grid_forget()
		self.frame.grid()

		#self.root.destroy()
		#self.root.quit()	

	def DataParser(self, text):
		self.textarea.config(state=NORMAL)
		msg = text.split(':')
		user = msg[0]
		message = msg[1]

		if message:
			if message == 'SIGNIN':
				self.textarea.insert(END,"\n"+user+" เข้าสู่ห้องสนทนา", "italic")
			elif message == 'SIGNOUT':
				self.textarea.insert(END,"\n"+user+" ออกจากห้องสนทนา", "italic")
			else:
				self.textarea.insert(END, user+' : \n', "bold")
				emoticons = re.compile(";(\d+);").findall(message)
				msgs = re.compile(";\d+;").sub("\n",message).split("\n")
				self.textarea.insert(END, '   ')
				i = 0
				for msg in msgs:
					self.textarea.insert(END, msg, "normal")
					if i < len(emoticons):
						self.emoticons_icon.append(PhotoImage(file="images/emoticon/"+str(emoticons[i])+".gif"))
						self.textarea.image_create(END, image=self.emoticons_icon[-1])
						i += 1

		else:
			self.textarea.insert(END, text)

		self.textarea.insert(END, "\n")
		self.textarea.yview_pickplace("end")
		self.textarea.config(state=DISABLED)

	def SendCommand(self):
		if (self.text_ent.get()):
			self.connection.send("MSG "+self.tospecial+self.text_ent.get().encode("utf-8"))
			self.text_ent.delete(0, END)
			if (self.tospecial):
				self.tospecial = ''
				self.listonline.select_clear(0, END)	

	def updateOnline(self, texto):
		uonlines = texto.split(",")
		self.listonline.delete(0, END)
		for u in uonlines:
			self.listonline.insert(END, u)

	def onselectUser(self, evt):
		w = evt.widget
		index = int(w.curselection()[0])
		value = w.get(index)
		pmto = value.encode("utf-8")
		newto = "PM "+pmto+" "
		print pmto+' '+self.username
		if (newto == self.tospecial) or (pmto == self.username):
			self.listonline.select_clear(index)
			newto = ''
		self.tospecial = newto

	def AddEmoticonButton(self, event):
		self.text_ent.insert(END, ";"+str(event.widget.value)+";")

	def LoginButton(self, event):
		self.login()

	def LogoutButton(self, event):
		self.logout()
		
	def aboutButton(self, event):
		tkMessageBox.showinfo("About", "Titiwat Kachayangyean\t53211520\nNitipat Lowichakornthikun\t53211525\nPreecha Chanruksa\t\t53211529\n\nCPE#24")


	def EnterToSend(self, event):
		self.SendCommand()	

class FetchNewData(threading.Thread):
	def __init__(self, socket):
		threading.Thread.__init__(self)
		self.stop_event = threading.Event()
		self.data = ''
		self.connection = socket

	def stop(self):
		self.stop_event.set()

	def run(self):
		while not self.stop_event.isSet():
			self.data = self.connection.recv(1024)
			msgs = self.data.split("\n")
			for msg in msgs:
				if (msg):
					if ('SHOWLIST' in msg):
						app.updateOnline(msg[9:])
					elif (msg == 'GETOUT'):
						self.stop()
						app.root.quit()
						break
					else:
						app.DataParser(msg)
		self.connection.close()

''' Main program '''
root = Tk()
app = ChatClientApp(root)
root.mainloop()