#!/usr/bin/python
#-*- coding: utf-8 -*-

from Tkinter import *
import tkMessageBox
import string, threading, socket, select
Clients = {'username':[], 'socket':[]}
class ChatServerApp:
	def __init__(self, master):
		self.root = master
		self.root.title("Server - Thitiwat Messenger")
		self.mainPage()
	def	mainPage (self):
		self.frame = Frame(self.root)
		self.textarea = Text(self.frame, height=15, width=40, bd=0)
		self.scroll = Scrollbar(self.frame, command=self.textarea.yview, bd=0)
		self.textarea.configure(yscrollcommand=self.scroll.set)
		self.host = Entry(self.frame)
		self.port = Entry(self.frame)
		self.v = StringVar()
		self.cmdButton = Button(self.frame, textvariable=self.v, command=self.cmdButton)
		self.v.set("Start")
		self.host.insert(END, socket.gethostbyname(socket.gethostname()))
		self.host.config(state=DISABLED)
		self.port.insert(END, "8000")
		self.frame.grid()
		self.host.grid(row=0,column=0)
		self.port.grid(row=0,column=1)
		self.cmdButton.grid(row=0,column=2)
		self.textarea.grid(row=1,column=0,columnspan=3)
		self.scroll.grid(row=1,column=3,sticky=N+S)
	def insertLog(self, text):
		self.textarea.insert(END, text+"\n")
	def cmdButton(self):
		if self.v.get() == 'Start':
			self.v.set("Stop")
			#self.cmdButton.config(state=DISABLED)
			self.insertLog("Server started...")
			try:
				connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				connection.bind((app.host.get(), int(app.port.get())))
				connection.listen(5)
			except:
				self.v.set("Start")
				self.insertLog("Server start failed (Port ploblem???)")
				return
				
			self.recLoop = recieveNewClient(connection)
			self.recLoop.start()				
			
		else:
			self.v.set("Start")
			self.insertLog("Server stopped...")
			self.recLoop.stop()
			

class recieveNewClient(threading.Thread):
	def __init__(self, socket):
		threading.Thread.__init__(self)
		self.stop_event = threading.Event()
		self.my_socket = socket
		Clients['username'] = []
		Clients['socket'] = []
		
	def stop(self):
		self.stop_event.set()
		
	def run(self):
		while not self.stop_event.isSet():
			rr,rw,err = select.select([self.my_socket],[],[], 1)
			if rr:
				connection, addr = self.my_socket.accept()
				newClient(connection).start()
		for C in Clients['socket']:
			C.send("GETOUT")
		self.my_socket.close()
		
class newClient(threading.Thread):
 
	def __init__(self, socket):
		threading.Thread.__init__(self)
		self.stop_event = threading.Event()
		self.connection = socket
		self.UserAdded = False
		self.data = ''

	def stop(self):
		self.stop_event.set()

	def run(self):
		while not self.stop_event.isSet():
			self.data = self.connection.recv(1024)
			#if app.v.get() == 'Start':
			#	break
			if 'ADDUSER' in self.data:
				if(self.UserAdded == False):
					self.UserAdded = True					
					Clients['username'].append(self.data[8:])
					Clients['socket'].append(self.connection)
					for C in Clients['socket']:
						if C != self.connection:
							C.send(self.data[8:]+":SIGNIN")
						C.send("\nSHOWLIST "+",".join(Clients['username']))
				app.insertLog(self.data[8:]+" signin...")

	 		if self.UserAdded:
				if 'END' in self.data:
					for C in Clients['socket']:
						if C == self.connection:
							username = Clients['username'][Clients['socket'].index(C)]
							Clients['username'].remove(username)
							Clients['socket'].remove(C)
							self.UserAdded = False
					for C in Clients['socket']:
						C.send(username+":SIGNOUT\nSHOWLIST "+",".join(Clients['username']))
					self.stop()
					app.insertLog(username+" signout...")

				elif 'MSG PM' in self.data:
					msg = self.data[7:].split()
					for i in Clients['username']:
						if i == msg[0]:
							del msg[0]
							message = string.join(msg, ' ')
							self.connection.send(Clients['username'][Clients['socket'].index(self.connection)]+" (Private ~ "+i+") :"+message)
							Clients['socket'][Clients['username'].index(i)].send(Clients['username'][Clients['socket'].index(self.connection)]+" (Private) :"+message)
							break

		 			app.insertLog(Clients['username'][Clients['socket'].index(self.connection)]+" to "+i+" >> "+message+"")

				elif 'MSG' in self.data:
					for C in Clients['socket']:
						C.send(Clients['username'][Clients['socket'].index(self.connection)]+":"+self.data[4:])
					app.insertLog(Clients['username'][Clients['socket'].index(self.connection)]+" >> "+self.data[4:]+"")
		 
				
	 	self.connection.close()

root = Tk()
app = ChatServerApp(root)
root.mainloop()